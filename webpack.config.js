var webpack = require('webpack');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require('path');

module.exports = {
  entry: {
    'bundle.js': [
      './src/app.tsx',
      './src/push.tsx',
      './src/siri.tsx'
    ]
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.png']
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader'
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.(woff|eot|ttf)$/,
        use: 'file-loader'
      },
      {
        test: /\.(png|svg|jpg)$/,
        use: 'url-loader'
      }
    ]
  },
  devtool: 'inline-source-map',
  plugins: [
    new CopyWebpackPlugin(
      [
        {from: './index.html'},
        {from: './landscape.html'},
        {from: './portrait.html'},
        {from: './pushNotifications.html'},
        {from: './siriDisplay.html'},
        {from: 'docs', to: 'docs'}
      ]),
    new webpack.optimize.UglifyJsPlugin()
  ]
}