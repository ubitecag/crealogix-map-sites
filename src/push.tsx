import * as $ from 'jquery';

let playersId: string;
let appId: string;
let restApiKey: string;
let allInfos: boolean = true;

const getUrlParameter = function getUrlParameter(sParam: string): string {
  const sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&');
  let sParameterName;
  // tslint:disable-next-line
  for (let i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      if (sParameterName[1] === undefined) {
        allInfos = false;
        return '';
      } else {
        return sParameterName[1];
      }
    }
  }
  allInfos = false;
  return '';
};

$(window).on('load', () => {
  const pId = getUrlParameter('players_id');
  const aId = getUrlParameter('app_id');
  const raKey = getUrlParameter('rest_api_key');
  playersId = pId === '' ? 'please provide players id' : pId;
  appId = aId === '' ? 'please provide app id' : aId;
  restApiKey = raKey === '' ? 'please provide a rest api key' : raKey;
  $('#app_id').append(appId);
  $('#players_id').text(playersId);
  $('#rest_api_key').text(restApiKey);
  if (allInfos) {
    $('#sending').show();
  }

  $('#submit').click(() => {
    const heading = $('#heading').val();
    const content = $('#content').val();
    const badgeType = $('#badgeType').val();
    const badgeNum = $('#badgeNum').val();
    const link = $('#link').val();
    const customLink = $('#customLink').val();
    if (validate(heading, content, badgeNum, badgeType, link, customLink)) {
      let data = {};
      if (link !== 'None') {
        if (customLink === '') {
          data = 'https://www.crealogix.com';
        } else {
          data = customLink;
        }
      }

      const message = {
        app_id: appId,
        include_player_ids: [playersId],
        contents: { en: content },
        headings: { en: heading },
        ios_badgeType: badgeType,
        ios_badgeCount: badgeNum,
        data
      };
      const headers = {
        'Content-Type': 'application/json; charset=utf-8',
        Authorization: 'Basic ' + restApiKey
      };

      fetch('https://onesignal.com/api/v1/notifications', {
        body: JSON.stringify(message),
        headers,
        cache: 'no-cache',
        method: 'POST',
        mode: 'cors'
      }).then((response) => {
        console.log(response);
      }).catch((error) => {
        console.log(JSON.stringify(error));
      });
    }
  });
  $('#link').change((a: any) => {
    const customLink = $('#customLink');
    customLink.prop('disabled', true);
    if (a.target.value === 'custom') {
      customLink.prop('disabled', false);
    }
  });

  $('#badgeType').change((a: any) => {
    const customLink = $('#badgeNum');
    customLink.prop('disabled', true);
    if (a.target.value !== 'None') {
      customLink.prop('disabled', false);
    }
  });
});

const validate = (heading: string, content: string, badgeNum: string, badgeType: string, link: string, customLink: string): boolean => {
  if (heading === '') {
    return false;
  }
  if (content === '') {
    return false;
  }
  if (badgeType !== 'None') {
    if (badgeNum === '') {
      return false;
    }
  }
  if (link !== 'None' && link !== 'crealogix') {
    if (customLink === '') {
      return false;
    }
  }
  return true;
};