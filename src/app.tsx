import {
  IPluginRegistry,
  DialogModule
} from '@map/core';
import {
  MapBase
} from '@map/application';
import {
  BrowserModule,
} from '@map/secure-browser';

import { CameraModule, GetCameraImageAsBase64Response } from '@map/camera';

/**
 * The application class defines what MAP modules are being used by the webapp.
 */
class Application extends MapBase {

  protected configureRegistrar(registrar: IPluginRegistry): void {
    // Call 'use' to include and set up a module
    DialogModule.use(registrar);
    BrowserModule.use(registrar);
    CameraModule.use(registrar);
  }
}

// Create an application instance
const application = new Application();

// When the close button is pressed, we access the services for
// showing dialogs or closing the secure browser.
export const onClose = function () {
  application.ready().then((provider) => {
    DialogModule.getService(provider).confirm('Please confirm', 'Do you want to close the page?').then((response) => {
      if (response.success) {
        BrowserModule.getService(provider).closePage();
      }
    });
  });
};

const onCSVOpen = function () {
  application.ready().then((provider) => {
    DialogModule.getService(provider).confirm('Please confirm', 'Do you want to open  the page in external browser?').then((response) => {
      if (response.success) {
        BrowserModule.getService(provider).openExternalPage("http://insight.dev.schoolwires.com/HelpAssets/C2Assets/C2Files/C2ImportCalEventSample.csv");
      }
    });
  });
};

const openCamera = function() {
  log('openCamera() - TRACE')('before application.ready()');
  application.ready().then((provider) => {
    log('openCamera() - TRACE')('application.ready()');

    try {
      CameraModule.getService(provider)
        .getImageAsBase64()
        .then((response: GetCameraImageAsBase64Response) => {
          log('onOpenCamera - INFO')(`Image path: ${ JSON.stringify(response) }`);
        })
        .catch((code) => {
          log(`onOpenCamera - INFO`)(`Available errors: ${ JSON.stringify(CameraModule.Errors) }`);
          log(`onOpenCamera - ERROR ${code}`)(code);
        });

        log('openCamera() - TRACE')('exit');
    } catch (error) {
      log('CameraModule.getService() - ERROR')(error);
    }
  });
};

const log = (label: String) => (info:any) => {
  const errorLogElement = document.getElementById('error-log');
  if (errorLogElement) {
    errorLogElement.innerHTML += `<p>On ${ label } | ${ JSON.stringify(info) }</p>`;
  } 
  console.error(`On ${ label } | ${ info }`);
}

// Register onClose() as the button event.
const addButtonEvents = function () {
  const closeButton = document.getElementById('mdb-close-action');
  const csvButton = document.getElementById('mdb-external-browser-action');
  const cameraButton = document.getElementById('js-open-camera');

  if (closeButton) {
    log('registerCloseButtonEvent - TRACE')('before application.ready()');
    application.ready().then((provider) => {
      log('registerCloseButtonEvent - TRACE')('application.ready()');
      closeButton.style.cursor = 'pointer';
      closeButton.addEventListener('click', onClose);
      log('registerCloseButtonEvent - TRACE')('onClose() is registered');
    }).catch(log('registerCloseButtonEvent - ERROR'));
    log('registerCloseButtonEvent - TRACE')('end application.ready()');
  } else {
    log('registerCloseButtonEvent - FATAL')('closeButton cannot be found');
  }

  if (csvButton) {
    log('registerCloseButtonEvent - TRACE')('before application.ready()');
    application.ready().then((provider) => {
      log('registerCsvButtonEvent - TRACE')('application.ready()');
      csvButton.style.cursor = 'pointer';
      csvButton.addEventListener('click', onCSVOpen);
      log('registerCsvButtonEvent - TRACE')('onCSVOpen is registered');
    }).catch(log('registerCsvButtonEvent - ERROR'));
    log('registerCloseButtonEvent - TRACE')('end application.ready()');
  } else {
    log('registerCsvButtonEvent - FATAL')('csvButton cannot be found');
  }

  if (cameraButton) {
    log('registerCameraButtonEvent - TRACE')('before application.ready()');
    application.ready().then((provider) => {
      log('registerCameraButtonEvent - TRACE')('application.ready()');
      cameraButton.addEventListener('click', openCamera);
      log('registerCameraButtonEvent - TRACE')('openCamera is registered');
    }).catch(log('registerCameraButtonEvent - ERROR'));
    log('registerCameraButtonEvent - TRACE')('end application.ready()');
  } else {
    log('registerCameraButtonEvent - FATAL')('cameraButton cannot be found');
  }
};


document.addEventListener('DOMContentLoaded', addButtonEvents, false);