import * as $ from 'jquery';
import {onClose} from './app';


let allInfos: boolean = true;

const getUrlParameter = function getUrlParameter(sParam: string): string {
  const sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&');
  let sParameterName;
  // tslint:disable-next-line
  for (let i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      if (sParameterName[1] === undefined) {
        allInfos = false;
        return '';
      } else {
        return sParameterName[1];
      }
    }
  }
  allInfos = false;
  return '';
};

$(window).on('load', () => {
  const siAm = getUrlParameter('siri_amount');
  const siPa = getUrlParameter('siri_payee');
  const siNo = getUrlParameter('siri_note');
  let siriAmount: string = siAm === '' ? 'please provide amout of your siri input' : siAm;
  let siriPayee: string = siPa === '' ? 'please provide payee of your siri input' : siPa;
  let siriNote: string = siNo === '' ? 'please provide note of your siri input' : siNo;
  $('#siri_amount').append(siriAmount);
  $('#siri_payee').text(siriPayee);
  $('#siri_note').text(siriNote);
  $("#siri_close").on('click', function () {
    console.log("hello");
    onClose();
  });
});
const close = document.getElementById('siri_close');
if (close !== null) {
  close.onclick = () => {
    console.log("hello");
  };
}

