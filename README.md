# map-sites
Map Sites is a showcase application that uses the SecureBrowser and 
Dialog modules from **map-core**.

* To bootstrap the **map-core** services the application 
inherits from the `MapBase`. 
* To include and set up the modules it calls `use`.
* `getService` is used to get the main interfaces for the included modules.


